#!/bin/sh

# This file installs two client-side git hooks to automatically generate the .gitlab-ci.yml
# file by running the generate-gitlab-ci.yml whenever it was updated.

if [ ! -e .git/hooks/pre-commit ]; then
cat >.git/hooks/pre-commit <<EOL
#!/bin/sh

CHECK_CHANGES=\$(git diff --exit-code HEAD~1..HEAD generate-gitlab-ci.py)
if [ \$? -eq 1 ] ; then
  echo "generate .gitlab-ci.yml file"
  touch .commit
fi
exit
EOL
chmod +x .git/hooks/pre-commit
fi

if [ ! -e .git/hooks/post-commit ]; then
cat >.git/hooks/post-commit <<EOL
#!/bin/sh

if [ -e .commit ]; then
  rm .commit
  python generate-gitlab-ci.py
  git add .gitlab-ci.yml &> /dev/null
  git commit --amend -C HEAD --no-verify &> /dev/null
fi
exit
EOL
chmod +x .git/hooks/post-commit
fi