# What is this project about?

This project is used to make a (nightly) system test

## Trigger a specific branch for testing
If you want to test a single branch in any dune-module in interaction with the other
downstream modules, simply trigger a pipeline with the additional variables

```
CI_BUILD_REF_NAME=[branch/to/test]
```

If a repository does not have this branch, it defaults to master.

## Enforce a specific branch for testing in all modules
If you want to test and enforce a specific branch for all modules, you can set

```
DUNECI_BRANCH=[branch/to/test]
```

If a repository does not have this branch, it stops with a hard error.

## Add your own module to the test pipeline
There is a python script `generate-gitlab-ci.py` to generate the `.gitlab-ci.yml`,
simply add your module or your toolchain to the corresponding variables in the
header of the script.